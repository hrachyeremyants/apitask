import React, { Component } from 'react';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ReactFlagsSelect from 'react-flags-select';
import CountryList from '../../components/country/CountryList';
import * as countriesActions from '../../store/country/actions';
import * as countriesSelectors from '../../store/country/selectors';

class CategoryListPage extends Component {
    constructor(props) {
        super(props);
        autoBind(this);
        this.state = {
            form : {
                countryCode : '',
                postalCode :''
            }
        }
    }
    handleOnSelectFlag (countryCode){
        let {form} = this.state;
        form.countryCode = countryCode;
        this.setState({ form : form })
    }
    handleOnChange(e){
        e.preventDefault();

        let {form} = this.state;

        form[e.target.name] = e.target.value;

        this.setState({ form : form })
    }
    handleSubmit(e){
        e.preventDefault();

        this.props.fetchItem(this.state.form);
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-6 col-md-offset-3">
                    <div className="form-group">
                        <ReactFlagsSelect
                            countries={[
                                "AD","AR","AS","AT","AU","BD","BE","BG","BR","CA",
                                "CH","CZ","DE","DK","DO","ES","FI","FO","FR","GB","GF","GG",
                                "GL","GP","GT","GU","GY","HR","HU","IM","IN","IS","IT","JE",
                                "JP","LI","LK","LT","LU","MC","MD","MH","MK","MP","MQ","MX",
                                "MY","NL","NO","NZ","PH","PK","PL","PM","PR","PT","RE","RU",
                                "SE","SI","SJ","SK","SM","TH","TR","US","VA","VI","YT","ZA"
                                ]}
                            placeholder="Choose Country"
                            className="form-control"
                            showSelectedLabel={true}
                            onSelect={this.handleOnSelectFlag}
                        />
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" name="postalCode" onChange={this.handleOnChange} />
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary" onClick={this.handleSubmit}>
                            Submit
                        </button>
                    </div>
                </div>
                <div className="col-md-12">
                    <CountryList
                        places={this.props.places}
                    />
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        places: countriesSelectors.getItems(state)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchAllCategories: () => {
            dispatch(countriesActions.fetchAllItems())
        },
        fetchDocuments: (deleteCache) => {
            dispatch(countriesActions.fetchItems(deleteCache))
        },
        fetchItem: (data) => {
            dispatch(countriesActions.fetchItem(data))
        },
        unsetCurrentDocumentId: () => {
            dispatch(countriesActions.unsetCurrentItemId())
        },
        deleteDocument: (id) => {
            dispatch(countriesActions.deleteItem(id))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListPage);