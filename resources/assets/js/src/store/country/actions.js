import { browserHistory } from 'react-router';
import api from '../../services/api';
import _ from 'lodash';

import * as countriesSelectors from './selectors';

export const types = {
    CLEAR_CACHE : 'countries.CLEAR_CACHE',
    // FETCH_ALL_ITEMS_DONE: 'countries.FETCH_ALL_ITEMS_DONE',
    // FETCH_ITEMS_DONE: 'countries.FETCH_ITEMS_DONE',
    FETCH_ITEM_DONE: 'countries.FETCH_ITEM_DONE',
    SET_CURRENT_ITEM_ID: 'countries.SET_CURRENT_ITEM_ID',
};


export function setCurrentItemId(id) {
    return {
        type: types.SET_CURRENT_ITEM_ID,
        payload: {
            id
        }
    }
}

export function unsetCurrentItemId() {
    return {
        type: types.SET_CURRENT_ITEM_ID,
        payload: {
            id: null,
        }
    }
}

export function clearCache() {
    return {
        type: types.CLEAR_CACHE
    }
}

export function fetchAllItems() {
    return async (dispatch) => {
        try {
            let params = new Map();
            // params.set('language_id', language.get());

            let items = await api.get('/', params,false);

            dispatch(clearCache());
            dispatch({
                type: types.FETCH_ALL_ITEMS_DONE,
                payload: {
                    items
                }
            });
        } catch (e) {
            dispatch(e);
        }
    }
}

export function fetchItems(deleteCache = false) {
    return async (dispatch, getState) => {
        let state = getState();
        try {
            // Set additional params
            let params = new Map();
            let filters = countriesSelectors.getFilters(state);
            let sorter = countriesSelectors.getSorter(state);
            let pagination = countriesSelectors.getPagination(state);
            // params.set('language_id', language.get());
            // params.set('expand', 'users,groups');
            // params.set('name~', filters.searchTerm);
            // params.set('page_size', pagination.pageSize);
            // params.set('page_number', deleteCache ? 1 : pagination.currentPage);
            // params.set('sort_by', sorter.column);
            // params.set('sort_desc', sorter.descending);


            // GET request from API
            let [response, items] = await api.get('/', params,false,true);

            // Clear cache if deleteCache is enabled
            if (deleteCache) {
                dispatch(clearCache());
            }

            dispatch({
                type: types.FETCH_ITEMS_DONE,
                payload: {
                    totalPages: parseInt(response.headers.get('X-Total-Pages')),
                    items
                }
            });
        } catch (e) {
            dispatch(e);
        }
    }
}

export function deleteItem(id) {
    return async (dispatch) => {
        try {
            // DELETE request to API
            await api.delete('/evolution-chain/' + id);
            dispatch(fetchItems());
        } catch (e) {
            dispatch(exceptionsActions.process(e));
        }
    }
}

export function fetchItem(data) {
    return async (dispatch) => {
        try {
            let params = new Map();
            _.map(data,(item,i)=>{
                params.set(i, item);
            });

            let items = await api.get('/getPlace', params);
            dispatch({
                type: types.FETCH_ITEM_DONE,
                payload: {
                    items
                }
            });
        } catch (e) {
            // dispatch(e);
        }
    };
    // return async (dispatch) => {
    //     try {
    //         let params = new Map();
    //         _.map(data,(item,i)=>{
    //             params.set(i, item);
    //         });
    //         let item = await api.get(`/getPlace`, params);
    //         console.log(item);
    //         dispatch({
    //             type: types.FETCH_ITEM_DONE,
    //             payload: {
    //                 item
    //             }
    //         })
    //     } catch (e) {
    //         dispatch(e);
    //     }
    // }
}