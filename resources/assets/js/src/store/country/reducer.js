import Immutable from 'seamless-immutable';
import { types } from './actions';
import moment from 'moment';
import _ from 'lodash';

const initialState = Immutable({
    currentItemId: null,
    itemsById: {
        // _id: {item_details}
    }
});

// Set the id of current item
function setCurrentItemId(state, payload) {
    return state.merge({
        currentItemId: payload.id
    })
}

// Clear cached info
function clearCache(state) {
    return state.merge({
        itemsById: {}
    })
}

function fetchItemDone(state,payload) {
    return state.merge({
        itemsById: payload.items
    })
}

// Save items to store
function fetchAllItemsDone(state, payload) {
    let items = [];
    let i = 0;
    for (let key in payload.items ){
        items.push({
            id : i,
            name : key,
            [key]:payload.items[key]
        })
        i++;
    }
    return state.merge({
        itemsById: items
    })
}

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.CLEAR_CACHE:
            return clearCache(state);

        case types.SET_CURRENT_ITEM_ID:
            return setCurrentItemId(state, action.payload);

        case types.FETCH_ITEM_DONE:
            return fetchItemDone(state, action.payload);

        default:
            return state;
    }
}