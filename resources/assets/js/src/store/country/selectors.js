import _ from 'lodash';

export function getItems(state) {
    return state.countries.itemsById;
}

export function getItemsByPage(state, page) {
    if (!state.countries.idsByPage['_' + page]) {
        page = (getPagination(state)).previousPage;
    }
    return _.map(state.countries.idsByPage['_' + page], (itemId) => {
        return state.countries.itemsById['_' + itemId]
    })
}

export function getItemById(state, id) {
    return state.countries.itemsById['_' + id];
}

export function getCurrentItem(state) {
    return state.countries.currentItemId ? getItemById(state, state.countries.currentItemId) : null;
}

export function getFilters(state) {
    return state.countries.filters;
}

export function getPagination(state) {
    return state.countries.pagination;
}

export function getSorter(state) {
    return state.countries.sorter;
}