import React, { Component } from 'react';
import autoBind from 'react-autobind';
import PropTypes from 'prop-types';
import _ from 'lodash';

class CountryList extends Component{

    constructor(props) {
        super(props);
        autoBind(this);
    }
    getTableBody(){
        return (
            <tr>
                <td>{this.props.places.hasOwnProperty('country_zip_code') ? this.props.places.country_zip_code.name : null}</td>
                <td>{this.props.places.hasOwnProperty('country_zip_code') ?  this.props.places.country_zip_code.postalCode : null}</td>
                <td>{this.props.places.hasOwnProperty('state') ?  this.props.places.state : null}</td>
                <td>{this.props.places.hasOwnProperty('state_abbreviation') ?  this.props.places.state_abbreviation : null}</td>
                <td>{this.props.places.hasOwnProperty('place_name') ?  this.props.places.place_name : null}</td>
                <td>{this.props.places.hasOwnProperty('latitude') ?  this.props.places.latitude : null}</td>
                <td>{this.props.places.hasOwnProperty('longitude') ?  this.props.places.longitude : null}</td>
            </tr>
        )
    }
    getList(){
        if (this.props.places.hasOwnProperty('country_zip_code')){
            return(
                <table className="table">
                    <thead>
                    <tr>
                        <th>Country</th>
                        <th>Zip code</th>
                        <th>State</th>
                        <th>State Abbreviation</th>
                        <th>Place Name</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.getTableBody()}
                    </tbody>
                </table>
            )
        }
        return null;
    }
    getNothingFound(){
        return(
            <div className="row text-center">
                <h1 className="alert alert-danger">Nothing Found</h1>
            </div>
        )
    }
    render(){
        console.clear();
        return (
            <div>
                {this.getList() ? this.getList() : this.getNothingFound()}
            </div>
        )
    }
}
CountryList.propTypes = {
    places: PropTypes.object.isRequired
};

export default CountryList;