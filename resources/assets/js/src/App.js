import classNames from 'classnames';
import React, { Component } from 'react';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

class App extends Component {

    constructor(props) {
        super(props);
        autoBind(this);
    }

    render() {
        let children = this.props.children;

        return (
            <div>
                <div className="container transation-2s-in-out">
                    { children }
                </div>
            </div>
        );
    }
}

export default App;