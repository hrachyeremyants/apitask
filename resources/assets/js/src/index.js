import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import thunk from 'redux-thunk';

import App from './App';

import CountryListPage from './containers/country/CountryListPage';

import * as reducers from './store/reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    combineReducers(reducers),
    composeEnhancers(applyMiddleware(thunk))
);
ReactDOM.render(
    <Provider store={ store }>
        <Router history={ browserHistory }>
            <Route component={ App }>
                <Route path='/' component={ CountryListPage } />
            </Route>
        </Router>
    </Provider>,
    document.getElementById('root')
);
