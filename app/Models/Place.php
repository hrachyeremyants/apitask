<?php

namespace App\Models;

use App\CountryZipCode;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'place';

    protected $primaryKey = "id";

    protected $fillable = ['place_name','state','state_abbreviation','latitude','longitude','zip_code_id'];

    const CREATED_AT = false;

    const UPDATED_AT = false;

    public function countryZipCode()
    {
        return $this->belongsTo(CountryZipCode::class,'country_id','id');
    }
}
