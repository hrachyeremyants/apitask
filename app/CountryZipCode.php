<?php

namespace App;

use App\Models\Place;
use Illuminate\Database\Eloquent\Model;

class CountryZipCode extends Model
{
    protected $table = 'country_zip_code';

    protected $primaryKey = "id";

    protected $fillable  = ['code','name','postalCode'];

    const CREATED_AT = false;

    const UPDATED_AT = false;

    public function places()
    {
        return $this->hasMany(Place::class,'country_id','id');
    }

}
