<?php

namespace App\Http\Controllers\API;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

use App\CountryZipCode;
use App\Models\Place;
use Mockery\Exception;

class CountryController extends Controller
{
    protected $client;

    protected $url = 'http://api.zippopotam.us/';

    public function  __construct()
    {
        $this->client = new Client(['base_uri'=>$this->url]);
    }

    public function getPlace(Request $request)
    {
        $place = Place::select('*')
                ->join('country_zip_code','country_zip_code.id','=','place.country_id')
                ->where('country_zip_code.code',$request->input('countryCode'))
                ->where('country_zip_code.postalCode',$request->input('postalCode'));
        $place = $place->first();
//        dd($place->toArray());
        if ($place == null){
            $this->getPlacesFromApi($request);
        }
        $countryCipCode = $place->countryZipCode()->first()->toArray();
        $place = $place->toArray();
        $place['country_zip_code'] = $countryCipCode;
        return response()->json(['data'=>$place]);
    }

    protected function getPlacesFromApi(Request $request)
    {
        try {
            $res = $this->client
                ->request('GET', $request->input('countryCode').'/'.$request->input('postalCode'));
            $data = json_decode($res->getBody());
            $newCountryZipCodeId = CountryZipCode::insertGetId([
                'name'=>$data->country,
                'code'=>$data->{'country abbreviation'},
                'postalCode'=>$data->{'post code'}
                ]);
            foreach ($data->places as $place){
                Place::insert([
                    'place_name'=>$place->{'place name'},
                    'state'=>$place->state,
                    'state_abbreviation'=>$place->{'state abbreviation'},
                    'latitude'=>$place->latitude,
                    'longitude'=>$place->longitude,
                    'country_id'=>$newCountryZipCodeId
                ]);
            }
            return $this->getPlace($request);
        }catch (ClientException $clientException){
            print(json_encode(['data'=>(object)$clientException->getHandlerContext()]));
            die;
        }
    }

    protected function error()
    {
        return response()->json(['data'=>[]]);
    }
}
